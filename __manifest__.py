# -*- coding: utf-8 -*-
{
    'name': "personnel",

    'summary': """
        devloped by compangy MIC""",

    'description': """
        Long description of module's purpose
    """,

    'author': "MIC",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    'sequence': '1',
    # any module necessary for this one to work correctly
    'depends': ['base','mail'],

    # always loaded
    'data': [
         'security/ir.model.access.csv',
         'data/sequence.xml',
         'data/mail_templates.xml',
         'reports/report.xml',
         'reports/personnel_card.xml',



        'views/information/views_situationFamiliale.xml',
        'views/information/views_personnel.xml',
        'views/information/views_personneContacter.xml',

        'views/retraite/views_retraite.xml',

        'views/employee/views_scolarité.xml',
        'views/employee/views_recrutement.xml',
        'views/employee/views_poste.xml',

        'views/administration/views_service.xml',
        'views/administration/views_mutuelle.xml',
        'views/administration/views_division.xml',

        'views/employee/views_grade.xml',
        'views/employee/views_echelon.xml',
        'views/employee/views_echelle.xml',
        'views/employee/views_ancienEmploi.xml',
        'views/employee/views_typeFonctionnaire.xml',

        'views/congee/views_conge.xml',
        'views/congee/views_demandecongee.xml',


        'views/absance/views_feuillepresence.xml',
        'views/absance/views_sanction.xml',
        'views/absance/views_comissiondiciplinaire.xml',
        'views/absance/views_membre.xml',
        'views/absance/views_action.xml',


        'views/concour/views_personneinscrits.xml',
        'views/concour/views_concour.xml',
        'views/concour/views_comissionorale.xml',
        'views/concour/views_membre1.xml',
        'views/concour/views_comissionfinale.xml',
        'views/concour/views_publication.xml',



    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
}