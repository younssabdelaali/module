# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Action(models.Model):
     _name = 'ressource.action'

     type = fields.Char('Type')
     DateExécution  = fields.Date('Date d’exécution ')
     delaiReponse  = fields.Integer('Délai réglementaire de réponses ')
     Statut = fields.Char('Statut')
     Réponses  = fields.Char('Réponses ')
     Observations = fields.Text('Observations')




     # les relations One2many

     # les relations Many2one
     membre_id = fields.Many2one(comodel_name='ressource.membre')
     comissiondiciplinaire_id = fields.Many2one(comodel_name='ressource.comissiondiciplinaire')






     #les relations Many2one

     #  mutelle_id = fields.Many2one(comodel_name='ressource.mutuelle')
     # grade_id = fields.Many2one(comodel_name='ressource.grade')
     # service_id = fields.Many2one(comodel_name='ressource.service')


     # les relations One2many

     #  situationFamiliale_ids = fields.One2many(comodel_name='ressource.situationfamiliale', inverse_name='personnel_id')
     #  echelle_ids = fields.One2many(comodel_name='ressource.echelle', inverse_name='personnel_id')

     # les relation ManyTomany
     #   poste_ids = fields.Many2many(comodel_name='ressource.poste',
     #            relation='personnel_poste_rel',
     #            column1='matricule ',
     #                             column2='nome')

     #   typefonctionnaire_ids = fields.Many2many(comodel_name='ressource.typefonctionnaire',
     #                                   relation='personnel_typefonctionnaire_rel',
     #                                  column1='matricule',
     #                                  column2='type')

