# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Retraite(models.Model):
     _name = 'ressource.retraite'
     RégimeRetraite  = fields.Selection([("CNSS","CNSS"),("CIMR","CIMR"),("RCAR","RCAR"),("CMR","CMR")])
     numAffiliation   = fields.Char("N° d’affiliation ")
     DateAffiliation = fields.Datetime("Date d’affiliation")


