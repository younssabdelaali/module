# -*- coding: utf-8 -*-

from odoo import models, fields, api , _

class  Scolarite(models.Model):
     _name = 'ressource.scolarite'
     _rec_name = 'diplome'
     niveauAcadémiquename = fields.Selection([("bac","Baccalauréat"),("bac+2","BAC +2"),("bac+3","Bac +3"),("bac+4","BAC +4"),("bac+5","Bac +5"),("autres","Autre")],"niveau Académique")
     diplome=fields.Selection([("DT","Diplôme de Technicien"),("DTS","Diplôme de Technicien Spécialisé"),("BTS","Brevet de Technicien Supérieur"),("DUT","Diplôme universitaire de Technologie"),("DEUST","Diplôme d’Etudes Universitaires Scientifiques et Techniques"),("DEUG","Diplôme d'études universitaires générales"),
                               ("LST","Licence Sciences et Techniques"),("LP","Licence Professionnelle"),("LF","Licence Fondamentale"),
                               ("IE","Ingénieur d'Etat"),("MST","Master Sciences et Techniques"),("MR+5","Master Recherche"),("MS","Master Spécialisé"),("Doctorat","Doctorat")],string="Diplome")


     #personneinscrits_id = fields.Many2one(comodel_name='ressource.personneinscrits')
     personneinscrits_ids = fields.One2many(comodel_name='ressource.personneinscrits', inverse_name='scolarite_id')



     personneinscrits_count=fields.Integer("personne inscrits",compute='get_personneinscrits_count')
     def get_personneinscrits_count(self):
         count = self.env['ressource.personneinscrits'].search_count([('scolarite_id','=',self.id)])
         self.personneinscrits_count = count

     @api.multi
     def personneinscrits(self):
          return {
               'name': _('personneinscrits'),
               'domain': [('scolarite_id', '=', self.id)],
               'view_type': 'form',
               'view_mode': 'tree,form',
               'res_model': 'ressource.personneinscrits',
               'view_id': False,
               'type': 'ir.actions.act_window',
          }




#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100