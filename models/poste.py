# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Poste(models.Model):
     _name = 'ressource.poste'

     nome = fields.Char('nom de poste')
     salaire = fields.Float()

    #les relation ManyTomany
     personnel_ids = fields.Many2many(comodel_name='ressource.personnel',
                                      relation='poste_personnel_rel',
                                      column1='nome',
                                      column2='matricule')


