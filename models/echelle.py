# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Echelle(models.Model):
     _name = 'ressource.echelle'
     niveau = fields.Selection([("echelle 6", "Echelle 6"), ("echelle 7", "Echelle 7"),("echelle 8", "Echelle 8"),("echelle 9", "Echelle 9"),("echelle 10", "Echelle 10"),("echelle 11", "Echelle 11"),("horsEchell", "Hors echelle")])


     #les relations Many2one
     personnel_id = fields.Many2one(comodel_name='ressource.personnel')



#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100