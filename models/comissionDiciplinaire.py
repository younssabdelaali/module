# -*- coding: utf-8 -*-

from odoo import models, fields, api , _

class  ComissionDiciplinaire(models.Model):
     _name = 'ressource.comissiondiciplinaire'

     dateReunion = fields.Char('date de Reunion')
     pv  = fields.Float('PV')
     pieceJointes = fields.Text('pieceJointes')



     #les relations One2many
     sanction_ids= fields.One2many(comodel_name='ressource.sanction', inverse_name='comissiondiciplinaire_id')
     action_ids = fields.One2many(comodel_name='ressource.action', inverse_name='comissiondiciplinaire_id')

     membre_ids = fields.One2many(comodel_name='ressource.membre', inverse_name='comissiondiciplinaire_id')

     @api.multi
     def membre(self):
          return {
               'name': _('Member'),
               'domain': [('comissiondiciplinaire_id', '=', self.id)],
               'view_type': 'form',
               'view_mode': 'tree,form',
               'res_model': 'ressource.membre',
               'view_id': False,
               'type': 'ir.actions.act_window',
          }

     @api.multi
     def action(self):
          return {
               'name': _('Action'),
               'domain': [('comissiondiciplinaire_id', '=', self.id)],
               'view_type': 'form',
               'view_mode': 'tree,form',
               'res_model': 'ressource.action',
               'view_id': False,
               'type': 'ir.actions.act_window',
          }

     @api.multi
     def sanction(self):
          return {
               'name': _('Saction'),
               'domain': [('comissiondiciplinaire_id', '=', self.id)],
               'view_type': 'form',
               'view_mode': 'tree,form',
               'res_model': 'ressource.sanction',
               'view_id': False,
               'type': 'ir.actions.act_window',
          }

     action_count = fields.Integer("Feuille d'absance", compute='get_action_count')
     membre_count = fields.Integer("Feuille d'absance", compute='get_membre_count')
     sanction_count = fields.Integer("Feuille d'absance", compute='get_sanction_count')
         # calculer le nomber des sanction
     def get_sanction_count(self):
         count = self.env['ressource.sanction'].search_count([('comissiondiciplinaire_id','=',self.id)])
         self.sanction_count = count
     def get_membre_count(self):
         count = self.env['ressource.membre'].search_count([('comissiondiciplinaire_id','=',self.id)])
         self.membre_count = count
     def get_action_count(self):
         count = self.env['ressource.action'].search_count([('comissiondiciplinaire_id','=',self.id)])
         self.action_count = count


