# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from odoo.exceptions import ValidationError

class  Publication(models.Model):
     _name = 'ressource.publication'
     _order='datePublication desc'

     numéro = fields.Integer("numéro")
     type = fields.Char("type")
     datePublication= fields.Date("Date de publication ")
     dateFinPublication= fields.Date("Date de fin de la publication ")
     dateCondidatur= fields.Date("Date de dépôt de la demande de candidature ")

     # # les relation ManyTomany
     # concour_ids = fields.Many2many(comodel_name='ressource.concour',
     #                                  relation='publication_concour_rel',
     #                                  column1='type',
     #                                  column2='ecrit')


     # les relation ManyTomany
     concour_ids = fields.Many2many(comodel_name='ressource.concour',
                                      relation='publi_concour_rel',
                                      column1='numéro',
                                      column2='ecrit')

     @api.one
     @api.constrains('datePublication', 'dateFinPublication')
     def cheaking_dates(self):
         if self.datePublication > self.dateFinPublication:
             raise ValidationError(_('la date de pubilcation doit être obligatoire  inferieur à la date de fin de publication'))
