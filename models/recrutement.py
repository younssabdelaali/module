# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime


class  Recrutement(models.Model):
     _name = 'ressource.recrutement'

     image=fields.Binary(string="Image",attachment=True,related="personnel_id.image", readonly=True)
     matricule = fields.Char('Nom',related="personnel_id.matricule", readonly=True)
     prenom = fields.Char('Prenom',related="personnel_id.prenom", readonly=True)


     dateRecrutement = fields.Date('Date de recrutement')
     datePriseService = fields.Date('Date prise de service ')
     DurePrévueService = fields.Float('Durée prévue du service')
     DureReelService = fields.Integer('Durée réelle du service',compute="calcule")
     dateFinService = fields.Date('Date de fin de service')
     dateTitularisation = fields.Date('Date titularisation')



     def calcule(self):
          self.ensure_one()
          if self.dateTitularisation and self.dateFinService is not False:
               d1 = datetime.strptime(self.dateTitularisation, '%Y-%m-%d')
               d2 = datetime.strptime(self.dateFinService,'%Y-%m-%d')
               daysDiff = str((d2 - d1).days)
               # print(daysDiff)
               self.DureReelService=daysDiff
          return self.DureReelService
     # les relations Many2one
     personnel_id = fields.Many2one(comodel_name='ressource.personnel', required=True)

