# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  comissionOrale(models.Model):
     _name = 'ressource.comissionorale'

     datePublication= fields.Char('Date publication des résultats de l’oral ')
     pv  = fields.Text('PV commission résultats oral')
     pieceJointes = fields.Text('piece Jointes')





  # les relations One2many
     membre1_ids = fields.One2many(comodel_name='ressource.membre1', inverse_name='comissionfinale_id')

     @api.multi
     def name_get(self):
          result = []
          for rec in self:
               name = rec.nom + '  ' + rec.prenom
               result.append((rec.id, name))