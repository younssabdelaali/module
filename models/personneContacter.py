# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  PersonneContacter(models.Model):
     _name = 'ressource.personnecontacter'
     nom = fields.Char('Nom')
     prenom = fields.Char('Prenom')
     gsm = fields.Char('Télephone')
     matricule = fields.Char('Matricule',required=True)


     # les relations Many2one
     personnel_id = fields.Many2one(comodel_name='ressource.personnel', required=True)


#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100