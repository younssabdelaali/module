# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Personnel(models.Model):
     _name = 'ressource.personnel'
     _inherit = ['mail.thread', 'mail.activity.mixin']
     _rec_name = 'nom'

     image=fields.Binary(string="Image",attachment=True)
     # sequence file
     name_seq = fields.Char(string='Personnel ID', required=True, copy=False, readonly=True,
                     index=True, default=lambda self: _('New'))
     nom = fields.Char('Nom')
     prenom = fields.Char('Prenom')
     sexe = fields.Selection([("male", "Male"), ("female", "Female")])
     cin = fields.Char('CIN')
     dateNaissance = fields.Date('Date de Naissance')
     matricule = fields.Char('Matricule',required=True)
     email = fields.Char()
     nationalite = fields.Char('Nationalité' ,default='Marocaine')
     # adress = fields.Text('Adresse')
     telDomicile = fields.Char('telDomicile')
     gsm = fields.Char('Tél')

     chefservice = fields.Char('chef de service ',related="service_id.chefService", readonly=True)

     # ladresse
     street = fields.Char(string="Address")
     zip = fields.Char(string="code postale")
     city = fields.Char(string="Ville")
     state_id = fields.Many2one("res.country.state", string='Etat')
     country_id = fields.Many2one('res.country', string='pays')


     emp = fields.Boolean('Avez-vous déjà travailler ?')
     ancienPoste = fields.Char('Ancien poste',related="ancienemploi_id.ancienPoste", readonly=True)
     ancienRecruteur = fields.Char('Ancien recruteur',related="ancienemploi_id.ancienRecruteur", readonly=True)
     responsableHiérarchique = fields.Char('Responsable hiérarchique',related="ancienemploi_id.responsableHiérarchique", readonly=True)
     responsableFonctionnel = fields.Char('Responsable fonctionnel',related="ancienemploi_id.responsableFonctionnel", readonly=True)

     # active = fields.Boolean('Active?', default=True)
     echelle = fields.Selection([("echelle6", "Echelle 6"), ("echelle7", "Echelle 7"),("echelle8", "Echelle 8"),("echelle9", "Echelle 9"),("echelle10", "Echelle 10"),("echelle11", "Echelle 11"),("horsEchell", "Hors echelle")],default="echelle6")


     feuilleabsnace_count=fields.Integer("Feuille d'absance",compute='get_feuille_count')
     sanction_count=fields.Integer("Feuille d'absance",compute='get_sanction_count')

     #les relations Many2one

     mutelle_id = fields.Many2one(comodel_name='ressource.mutuelle')
     grade_id = fields.Many2one(comodel_name='ressource.grade')
     service_id = fields.Many2one(comodel_name='ressource.service')
     echelon_id = fields.Many2one(comodel_name='ressource.echelon')
     ancienemploi_id = fields.Many2one(comodel_name='ressource.ancienemploi',string="Ancien Emploi")


     # les relations One2many

     personnecontacter_ids = fields.One2many(comodel_name='ressource.personnecontacter', inverse_name='personnel_id')
     situationFamiliale_ids = fields.One2many(comodel_name='ressource.situationfamiliale', inverse_name='personnel_id')
     sanction_ids = fields.One2many(comodel_name='ressource.sanction', inverse_name='personnel_id')
     recrutement_ids = fields.One2many(comodel_name='ressource.recrutement', inverse_name='personnel_id')
     demandecongee_ids = fields.One2many(comodel_name='ressource.demandecongee', inverse_name='personnel_id')
     feuillePresence_ids = fields.One2many(comodel_name='ressource.feuillepresence', inverse_name='personnel_id')
     # echelon_ids = fields.One2many(comodel_name='ressource.echelon', inverse_name='personnel_id')

     # les relation ManyTomany
     poste_ids = fields.Many2many(comodel_name='ressource.poste',
                                 relation='personnel_poste_rel',
                                 column1='matricule ',
                                 column2='nome')

     typefonctionnaire_ids = fields.Many2many(comodel_name='ressource.typefonctionnaire',
                                      relation='personnel_typefonctionnaire_rel',
                                      column1='matricule',
                                      column2='type')



     @api.onchange('situationFamiliale_ids')
     def cheak_number(self):
         if len(self.situationFamiliale_ids) > 1:
             raise ValidationError(_('error un personne a une seul situation '))

     @api.onchange('recrutement_ids')
     def cheak_recrutement(self):
         if len(self.recrutement_ids) > 1:
             raise ValidationError(_("impossible de l'ajout"))

     @api.model
     def create(self, vals):
         if vals.get('name_seq', _('New')) == _('New'):
            vals['name_seq'] = self.env['ir.sequence'].next_by_code('personnel.sequence') or _('New')
         result = super(Personnel, self).create(vals)
         return result

     @api.multi
     def next_echelle(self):
         self.ensure_one()
         if self.echelle == 'echelle6':
             return self.write({'echelle': 'echelle7'})
         elif self.echelle == 'echelle7':
             return self.write({'echelle': 'echelle8'})
         elif self.echelle == 'echelle8':
             return self.write({'echelle': 'echelle9'})
         elif self.echelle == 'echelle9':
             return self.write({'echelle': 'echelle10'})
         elif self.echelle == 'echelle10':
             return self.write({'echelle': 'echelle11'})
         elif self.echelle == 'echelle11':
             return self.write({'echelle': 'horsEchell'})

         else:
             raise ValidationError(_(self.nom+' '+self.prenom+' '+'est déjà hors echelle'))

     @api.multi
     def send_mail(self):
         self.ensure_one()
         template_id = self.env.ref('ressource.email_template_pr').id
         ctx = {
             'default_model': 'ressource.personnel',
             'default_res_id': self.id,
             'default_use_template': bool(template_id),
             'default_template_id': template_id,
             'default_composition_mode': 'comment',
             'email_to': self.email,
         }
         return {
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'mail.compose.message',
             'target': 'new',
             'context': ctx,
         }
# redirection pour un autre module
     @api.multi
     def feuille(self):
         return {
             'name': _('feuille'),
             'domain': [('personnel_id','=',self.id)],
             'view_type': 'form',
             'view_mode': 'tree,form',
             'res_model': 'ressource.feuillepresence',
             'view_id': False,
             'type': 'ir.actions.act_window',
         }
     @api.multi
     def recrutement(self):
         return {
             'name': _('recrutement'),
             'domain': [('personnel_id','=',self.id)],
             'view_type': 'form',
             'view_mode': 'tree,form',
             'res_model': 'ressource.recrutement',
             'view_id': False,
             'type': 'ir.actions.act_window',
         }
     @api.multi
     def sanction(self):
         return {
             'name': _('Saction'),
             'domain': [('personnel_id','=',self.id)],
             'view_type': 'form',
             'view_mode': 'tree,form',
             'res_model': 'ressource.sanction',
             'view_id': False,
             'type': 'ir.actions.act_window',
         }
# calculer le nomber des asb
     def get_feuille_count(self):
         count = self.env['ressource.feuillepresence'].search_count([('personnel_id','=',self.id)])
         self.feuilleabsnace_count = count

         # calculer le nomber des sanction
     def get_sanction_count(self):
         count = self.env['ressource.sanction'].search_count([('personnel_id','=',self.id)])
         self.sanction_count = count

     # ==============================  les demandes ===========================================

     demande_count = fields.Integer("Feuille d'absance", compute='get_demande_count')

     def get_demande_count(self):
         count = self.env['ressource.demandecongee'].search_count([('personnel_id', '=', self.id)])
         self.demande_count = count

     @api.multi
     def demande(self):
         return {
             'name': _('Demande'),
             'domain': [('personnel_id', '=', self.id)],
             'view_type': 'form',
             'view_mode': 'tree,form',
             'res_model': 'ressource.demandecongee',
             'view_id': False,
             'type': 'ir.actions.act_window',
         }
