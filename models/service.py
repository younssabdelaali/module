# -*- coding: utf-8 -*-

from odoo import models, fields, api ,_
from odoo.exceptions import ValidationError

class  Service(models.Model):
     _name = 'ressource.service'
     _rec_name = 'nomService'

     nomService = fields.Char('nom de Service')
     chefService = fields.Char('chef de service ')


     # les relation One2many
     personnel_ids = fields.One2many(comodel_name='ressource.personnel', inverse_name='service_id')

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
