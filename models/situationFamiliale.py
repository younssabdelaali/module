# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  SituationFamiliale(models.Model):
     _name = 'ressource.situationfamiliale'


     #les attribut
     prenom = fields.Char('Prenom',related="personnel_id.prenom")

     # situationFamiliale = fields.Selection([("célibataire ","célibataire "),("marié ","marié"),("divorcé ","divorcé ")],default='marié')
     marier = fields.Boolean()
     célibataire = fields.Boolean()
     Date=fields.Datetime()
     nomConjoint= fields.Char('nomConjoint')
     prenomConjoint = fields.Char('prenom Conjoint')
     cinConjoint = fields.Char('CIN du conjoint ')
     adresseConjoint = fields.Text('Adresse du conjoint ')
     organismeConjoint = fields.Char('Organisme du conjoint ')

     nbrEnfantfeminin = fields.Integer("Nombre d’enfants féminin ")
     nbrEnfantMasculin = fields.Integer("Nombre d’enfants masculin ")

     #les relations Many2one
     personnel_id = fields.Many2one(comodel_name='ressource.personnel',required=True)




#les relation
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100