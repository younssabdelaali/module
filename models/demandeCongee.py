# -*- coding: utf-8 -*-

from odoo import models, fields, api ,_
from odoo.exceptions import ValidationError
from datetime import datetime

class  DemandeCongee(models.Model):
     _name = 'ressource.demandecongee'

     image = fields.Binary(string="Image", attachment=True, related="personnel_id.image",readonly=True)
     matricule = fields.Char('Nom', related="personnel_id.matricule",readonly=True)
     prenom = fields.Char('Prenom', related="personnel_id.prenom",readonly=True)

     state  = fields.Selection([("demandé", "Demandé"),("Encours", "Encours"),("traiter", "Traiter"),("validationsupérieur", "validation supérieur"),("validationadministration", "validation administration"),("accepter", "Accepter"), ("rejetée", "rejetée")],string="Statut", readonly=True, default='demandé')
     dateDebut = fields.Date('Date de début')
     dateRetour = fields.Date('Date de Retour')
     nbrDeJour = fields.Char("Nombre de jours demandé ",compute="calcule")
     motif  = fields.Text('Motif ')
     remarques   = fields.Text('Remarques ')
     intérimaireValidé     = fields.Text('Intérimaire validé par ')
     intérimaireDésigné   = fields.Text('RemIntérimaire désigné arques ')


     #les relation Many2one
     conge_id = fields.Many2one(comodel_name='ressource.conge', string="Congé")
     personnel_id = fields.Many2one(comodel_name='ressource.personnel', required=True)

     @api.one
     def calcule(self):
          # self.ensure_one()
          if self.dateDebut and self.dateRetour is not False:
               d1 = datetime.strptime(self.dateDebut, '%Y-%m-%d')
               d2 = datetime.strptime(self.dateRetour,'%Y-%m-%d')
               daysDiff = str((d2 - d1).days)
               # print(daysDiff)
               self.nbrDeJour=daysDiff
          return self.nbrDeJour


     def action_Encours(self):
          for rec in self:
               rec.state = 'Encours'
     def action_traiter(self):
          for rec in self:
               rec.state = 'traiter'

     def action_accepter(self):
          for rec in self:
               rec.state = 'accepter'

     def action_rejetée(self):
          for rec in self:
               rec.state = 'rejetée'
     def action_validationadministration(self):
          for rec in self:
               rec.state = 'validationadministration'

     def action_validationsupérieur(self):
          for rec in self:
               rec.state = 'validationsupérieur'

     @api.one
     @api.constrains('dateDebut', 'dateRetour')
     def cheaking_dates(self):
          if self.dateRetour < self.dateDebut:
               raise ValidationError(_(self.personnel_id.nom+' '+self.personnel_id.prenom+" veuillez s'il vous plaît vérifier les dates"))


