# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Sanction(models.Model):
     _name = 'ressource.sanction'
     _rec_name = 'Nature'

     image = fields.Binary(string="Image", attachment=True, related="personnel_id.image", readonly=True)
     matricule = fields.Char('Nom', related="personnel_id.matricule", readonly=True)
     prenom = fields.Char('Prenom', related="personnel_id.prenom", readonly=True)

     type = fields.Char('Type')
     Nature = fields.Char('Nature ')
     Objet = fields.Char('Objet ')
     Description = fields.Char('Description ')
     RetourAttendu = fields.Date('Retour attendu ')
     delaiRetour = fields.Date('Délai du retour ')
     Remarques = fields.Text('Remarques ')
     Pièces = fields.Char('Pièces jointes')



     # les relations Many2one
     personnel_id = fields.Many2one(comodel_name='ressource.personnel')
     comissiondiciplinaire_id = fields.Many2one(comodel_name='ressource.comissiondiciplinaire',string="Comission Diciplinaire")

     # les relations One2many




