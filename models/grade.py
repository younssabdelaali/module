# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Grade(models.Model):
     _name = 'ressource.grade'
     _rec_name = 'grade'

     grade = fields.Char('Grade')
     Remarques = fields.Text('	Remarques')
     numGrade = fields.Char('	N° du grade ')
     designation = fields.Char('	Désignation ')

     # les relation One2many
     personnel_ids = fields.One2many(comodel_name='ressource.personnel', inverse_name='grade_id')





