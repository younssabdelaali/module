# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  AncienEmploi(models.Model):
     _name = 'ressource.ancienemploi'
     _rec_name = 'ancienPoste'

     ancienPoste = fields.Char('Ancien poste')
     ancienRecruteur = fields.Char('Ancien recruteur')
     responsableHiérarchique = fields.Char('Responsable hiérarchique')
     responsableFonctionnel = fields.Char('Responsable fonctionnel')


     #les relations Many2one
     personnel_ids = fields.One2many(comodel_name='ressource.personnel', inverse_name='ancienemploi_id')

