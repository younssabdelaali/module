# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  PersonneInscrits(models.Model):
     _name = 'ressource.personneinscrits'

     nom = fields.Char('Nom')
     prenom = fields.Char('Prenom')
     sexe = fields.Selection([("male", "Male"), ("female", "Female")])
     cin = fields.Char('CIN')

     # les relations One2many
    # scolarite_ids = fields.One2many(comodel_name='ressource.scolarite', inverse_name='personneinscrits_id')
     scolarite_id = fields.Many2one(comodel_name='ressource.scolarite')

     concour_ids = fields.Many2many(comodel_name='ressource.concour',
                                        relation='personne_concour_rel',
                                        column1='nom',
                                        column2='ecrit')
