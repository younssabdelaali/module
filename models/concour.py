# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Concour(models.Model):
     _name = 'ressource.concour'


     ecrit = fields.Boolean("Concour Ecrit")
     orale = fields.Boolean("concour Oral")

     dateDebutEcrit = fields.Datetime("Date début écrit ")
     datefinEcrit = fields.Datetime("Date fin écrit ")


     dateDebut= fields.Datetime("Date début oral ")
     datefin= fields.Datetime("Date fin oral ")

     lieuE= fields.Text('Lieu de councour Ecrit')
     lieuO= fields.Text('Lieu de concoour orale')

     # publication_ids = fields.Many2many(comodel_name='ressource.publication',
     #                                    relation='concour_publication_rel',
     #                                    column1='ecrit',
     #                                    column2='type')

     personneinscrits = fields.Integer(string="nombre des personnes inscits", compute='comp_personneinscrits')

     def comp_personneinscrits(self):
         self.personneinscrits = len(self.personneinscrits_ids)

     # les relation ManyTomany
     publication_ids = fields.Many2many(comodel_name='ressource.publication',
                                      relation='concour_publi_rel',
                                      column1='ecrit',
                                      column2='numéro')

     personneinscrits_ids = fields.Many2many(comodel_name='ressource.personneinscrits',
                                      relation='concour_personne_rel',
                                      column1='ecrit',
                                      column2='nom')
