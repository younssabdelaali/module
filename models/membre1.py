# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Membre1(models.Model):
     _name = 'ressource.membre1'
     _rec_name = 'nom'

     nom = fields.Char('Nom')
     prenom = fields.Char('Prenom')
     sexe = fields.Selection([("male", "Male"), ("female", "Female")])
     cin = fields.Char('CIN')

     cmFinal=fields.Boolean("vous êtes membre du commission finale ?")
     cmOral =fields.Boolean("vous êtes membre du commission oral ?")
     cml =fields.Boolean("vous êtes membre du commission oral ?")
     cmr =fields.Boolean("vous êtes membre du commission oral ?")
   #  cmFinal=fields.Boolean("vous êtes membre du commission finale ?")

     # les relations Many2one
    # comissionexamenadmintech_id = fields.Many2one(comodel_name='ressource.comissionexamenadmintech')
     comissionfinale_id = fields.Many2one(comodel_name='ressource.comissionfinale',string="Commission Finale")
     comissionorale_id = fields.Many2one(comodel_name='ressource.comissionorale',string="Commission Oral")
     #comissionsuivisupervision_id = fields.Many2one(comodel_name='ressource.comissionsuivisupervision')
     #personneinscrits_id = fields.Many2one(comodel_name='ressource.personneinscrits',string="personnes inscrits")

