# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Ecrit(models.Model):
     _name = 'ressource.ecrit'

     dateDebut= fields.Datetime("Date début écrit ")
     datefin= fields.Datetime("Date fin écrit ")
     lieu= fields.Text('Lieu')

