# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from odoo.exceptions import ValidationError
from datetime import datetime


class FeuillePresence(models.Model):
     _name = 'ressource.feuillepresence'

     image = fields.Binary(string="Image", attachment=True, related="personnel_id.image", readonly=True)
     matricule = fields.Char('Nom', related="personnel_id.matricule", readonly=True)
     prenom = fields.Char('Prenom', related="personnel_id.prenom", readonly=True)

     date = fields.Date('Date')
     heureArrivée  = fields.Datetime('Heure d’arrivée ')
     heureSortie  = fields.Datetime('Heure de sortie  ')
     duréeAbsence= fields.Integer('Durée de l’absence ',compute="calcule")
     absenceJustifiée = fields.Boolean('Absence Justifiée  ')
     remarques = fields.Text('Remarques ')
     justification = fields.Text('justification')
     pièces = fields.Char('Pièces jointes')

     # les relations Many2one
     personnel_id = fields.Many2one(comodel_name='ressource.personnel', required=True)

     @api.one
     @api.constrains('heureSortie', 'heureArrivée')
     def cheaking_dates(self):
          if self.heureSortie > self.heureArrivée:
               raise ValidationError(_(self.personnel_id.nom + ' ' + self.personnel_id.prenom + " veuillez s'il vous plaît vérifier les dates"))

     @api.one
     def calcule(self):
          #self.ensure_one()
          if self.heureArrivée and self.heureSortie is not False:
               d1 = datetime.strptime(self.heureSortie, '%Y-%m-%d %H:%M:%S')
               d2 = datetime.strptime(self.heureArrivée,'%Y-%m-%d %H:%M:%S')
               daysDiff = str((d2 - d1).days)
               # print(daysDiff)
               self.duréeAbsence = daysDiff
          return self.duréeAbsence




