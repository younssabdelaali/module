# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Membre(models.Model):
     _name = 'ressource.membre'
     _rec_name = 'nom'


     nom = fields.Char('Nom')
     prenom = fields.Char('Prenom')
     sexe = fields.Selection([("male", "Male"), ("female", "Female")])
     cin = fields.Char('CIN')

     # les relations One2many
     action_ids = fields.One2many(comodel_name='ressource.action', inverse_name='membre_id')
     # les relations Many2one

     comissiondiciplinaire_id = fields.Many2one(comodel_name='ressource.comissiondiciplinaire')





