# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Division(models.Model):
     _name = 'ressource.division'
     _rec_name = 'namedivision'

     namedivision = fields.Char('nom de division')
     chefdivision = fields.Char('chef de division ')

     #les relation One2many
     personnel_ids = fields.One2many(comodel_name='ressource.personnel', inverse_name='service_id')

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100