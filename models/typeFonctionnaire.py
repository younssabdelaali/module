# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  TypeFonctionnaire(models.Model):

     _name = 'ressource.typefonctionnaire'

     type = fields.Selection([("رسمي", "رسمي"),("متعاقد", "متعاقد"),("موسمي", "موسمي"),("ملحق", "ملحق"), ("وضع رهن الاشارة", "وضع رهن الاشارة"), ("إنتقال", "إنتقال"), ("إستدعاء إداري", "إستدعاء إداري")])
     libellee = fields.Char('libellee')
     dureValidité = fields.Integer()
     remarque = fields.Text()



     # les relation ManyTomany
     personnel_ids = fields.Many2many(comodel_name='ressource.personnel',
                                      relation='typefonctionnaire_personnel_rel',
                                      column1='type',
                                      column2='matricule')

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100