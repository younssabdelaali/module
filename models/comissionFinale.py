# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ComissionFinale(models.Model):
     _name = 'ressource.comissionfinale'

     dateReslt = fields.Datetime('Date de publication des résultats finaux ')
     pv  = fields.Text('PV commission résultats finaux')
     Statut = fields.Text('Statut')
     Remarques  = fields.Text('Remarques ')
     pieceJointes = fields.Text('piece Jointes')


     # les relations One2many
     membre1_ids = fields.One2many(comodel_name='ressource.membre1', inverse_name='comissionfinale_id')

