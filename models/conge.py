# -*- coding: utf-8 -*-

from odoo import models, fields, api,_

class  Conge(models.Model):
     _name = 'ressource.conge'
     _rec_name = 'typeDeConge'

     typeDeConge = fields.Selection([("maladie", "Maladie"), ("réglementaire", "Réglementaire")],'Type de Conge')
     sousType = fields.Char('Sous type')
     libelle = fields.Char('Libellé')
     désignation  = fields.Char('Désignation')
     nbr = fields.Integer('Nombre de jours réglementaire')
     justificatif = fields.Text('justificatation')
     délai = fields.Char('Délai de réponse ')


     demande_count=fields.Integer("Feuille d'absance",compute='get_demande_count')

     def get_demande_count(self):
         count = self.env['ressource.demandecongee'].search_count([('conge_id','=',self.id)])
         self.demande_count = count



     # les relations One2many
     demandes_ids = fields.One2many(comodel_name='ressource.demandecongee', inverse_name='conge_id')

     @api.multi
     def demande(self):
          return {
               'name': _('Demande'),
               'domain': [('conge_id', '=', self.id)],
               'view_type': 'form',
               'view_mode': 'tree,form',
               'res_model': 'ressource.demandecongee',
               'view_id': False,
               'type': 'ir.actions.act_window',
          }



