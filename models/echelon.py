# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  Echelon(models.Model):
     _name = 'ressource.echelon'
     _rec_name = 'echelle'

     suvention = fields.Float('suvention')
     echelle = fields.Selection([("echelle6", "Echelle 6"), ("echelle7", "Echelle 7"),("echelle8", "Echelle 8"),("echelle9", "Echelle 9"),("echelle10", "Echelle 10"),("echelle11", "Echelle 11"),("horsEchell", "Hors echelle")])


  #les relations Many2one
     # personnel_id = fields.Many2one(comodel_name='ressource.personnel')
     # personnel_id = fields.Many2one(comodel_name='ressource.personnel',required=True)
     personnel_ids = fields.One2many(comodel_name='ressource.personnel', inverse_name='echelon_id')



#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100