# -*- coding: utf-8 -*-

from odoo import models, fields, api

class  ComissionExamenAdminTech(models.Model):
     _name = 'ressource.comissionexamenadmintech'

     pv = fields.Text('PV commission résultats écrit ')

     #les relations Many2one
     sanction_id = fields.Many2one(comodel_name='ressource.sanction')

  # les relations One2many
     membre1_ids = fields.One2many(comodel_name='ressource.membre1', inverse_name='comissionexamenadmintech_id')